import React from 'react';
import {SafeAreaView, StyleSheet, StatusBar} from 'react-native';
import Route from './src/application/Route/Route';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
const App = () => {
  return (
    <GestureHandlerRootView>
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <Route />
      </SafeAreaView>
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
});

export default App;
