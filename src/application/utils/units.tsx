export const Hectograms = (weight: number) => {
  return weight / 10;
};
export const Decimetres = (height: number) => {
  return height / 10;
};
