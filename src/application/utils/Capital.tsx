export const Capitalize = (item: string) => {
  return item.charAt(0).toUpperCase() + item.slice(1);
};
