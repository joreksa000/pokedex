import Normal from '../../persentasion/Assets/Image/svg/icons/normal.svg';
import Fire from '../../persentasion/Assets/Image/svg/icons/fire.svg';
import Water from '../../persentasion/Assets/Image/svg/icons/water.svg';
import Electric from '../../persentasion/Assets/Image/svg/icons/electric.svg';
import Grass from '../../persentasion/Assets/Image/svg/icons/grass.svg';
import Ice from '../../persentasion/Assets/Image/svg/icons/ice.svg';
import Fighting from '../../persentasion/Assets/Image/svg/icons/fighting.svg';
import Poison from '../../persentasion/Assets/Image/svg/icons/poison.svg';
import Ground from '../../persentasion/Assets/Image/svg/icons/ground.svg';
import Flying from '../../persentasion/Assets/Image/svg/icons/flying.svg';
import Psychic from '../../persentasion/Assets/Image/svg/icons/psychic.svg';
import Bug from '../../persentasion/Assets/Image/svg/icons/bug.svg';
import Rock from '../../persentasion/Assets/Image/svg/icons/rock.svg';
import Ghost from '../../persentasion/Assets/Image/svg/icons/ghost.svg';
import Dragon from '../../persentasion/Assets/Image/svg/icons/dragon.svg';
import Dark from '../../persentasion/Assets/Image/svg/icons/dark.svg';
import Steel from '../../persentasion/Assets/Image/svg/icons/steel.svg';
import Fairy from '../../persentasion/Assets/Image/svg/icons/fairy.svg';

const typeLogos = {
  normal: Normal,
  fire: Fire,
  water: Water,
  electric: Electric,
  grass: Grass,
  ice: Ice,
  fighting: Fighting,
  poison: Poison,
  ground: Ground,
  flying: Flying,
  psychic: Psychic,
  bug: Bug,
  rock: Rock,
  ghost: Ghost,
  dragon: Dragon,
  dark: Dark,
  steel: Steel,
  fairy: Fairy,
};


export type PokemonType = keyof typeof typeLogos;

export default typeLogos;