import {makeAutoObservable, action, runInAction} from 'mobx';
import axios from 'axios';
import {
  DetailedPokemon,
  Pokemon,
} from '../../infrastructure/entities/PageEntities';

class PokemonStore {
  pokemonList: DetailedPokemon[] = [];
  selectedPokemon: (DetailedPokemon | null)[] = [null, null];
  isLoading: boolean = false;
  error: string = '';
  currentPage: number = 1;
  limit: number = 25;
  limit2: number = 100;
  comparisonPokemonList: DetailedPokemon[] = [];

  constructor() {
    makeAutoObservable(this, {
      fetchPokemon: action,
      fetchPokemonDetails: action,
      setLoading: action,
      setError: action,
      setPokemonList: action,
      setSelectedPokemon: action,
      appendPokemonList: action,
      incrementPage: action,
      fetchComparisonPokemonList: action,
    });
  }

  setLoading(value: boolean) {
    this.isLoading = value;
  }

  setError(message: string) {
    this.error = message;
  }

  setPokemonList(pokemon: DetailedPokemon[]) {
    this.pokemonList = pokemon;
  }

  setSelectedPokemon(index: number, pokemon: DetailedPokemon | null) {
    this.selectedPokemon[index] = pokemon;
  }

  appendPokemonList(pokemon: DetailedPokemon[]) {
    this.pokemonList = [...this.pokemonList, ...pokemon];
  }

  incrementPage() {
    this.currentPage += 1;
  }

  fetchPokemon = async () => {
    this.setLoading(true);
    this.setError('');
    try {
      const response = await axios.get(`https://pokeapi.co/api/v2/pokemon`, {
        params: {
          limit: this.limit,
          offset: (this.currentPage - 1) * this.limit,
        },
      });
      const data = response.data;
      const detailedPokemonList = await Promise.all(
        data.results.map(async (pokemon: Pokemon) => {
          const detailsResponse = await axios.get(pokemon.url);
          const details = detailsResponse.data;
          return {
            id: details.id,
            name: details.name,
            sprites: details.sprites,
            base_experience: details.base_experience,
            height: details.height,
            weight: details.weight,
            abilities: details.abilities,
            types: details.types,
            cries: details.cries,
            stats: details.stats,
            order: details.order,
          };
        }),
      );
      runInAction(() => {
        if (this.currentPage === 1) {
          this.setPokemonList(detailedPokemonList);
        } else {
          this.appendPokemonList(detailedPokemonList);
        }
      });
    } catch (error) {
      runInAction(() => {
        this.setError('Failed to fetch Pokémon');
      });
    } finally {
      runInAction(() => {
        this.setLoading(false);
      });
    }
  };

  fetchComparisonPokemonList = async () => {
    this.setLoading(true);
    this.setError('');
    try {
      const response = await axios.get(`https://pokeapi.co/api/v2/pokemon`, {
        params: {
          limit: 100,
          offset: 0,
        },
      });
      const data = response.data;
      const detailedComparisonList = await Promise.all(
        data.results.map(async (pokemon: Pokemon) => {
          const detailsResponse = await axios.get(pokemon.url);
          const details = detailsResponse.data;
          return {
            id: details.id,
            name: details.name,
            sprites: details.sprites,
            base_experience: details.base_experience,
            height: details.height,
            weight: details.weight,
            abilities: details.abilities,
            types: details.types,
            cries: details.cries,
            stats: details.stats,
            order: details.order,
          };
        }),
      );
      runInAction(() => {
        this.comparisonPokemonList = detailedComparisonList;
      });
    } catch (error) {
      runInAction(() => {
        this.setError('Failed to fetch comparison Pokémon list');
      });
    } finally {
      runInAction(() => {
        this.setLoading(false);
      });
    }
  };

  fetchPokemonDetails = async (url: string) => {
    this.setLoading(true);
    this.setError('');
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw new Error('Failed to fetch Pokémon details');
    } finally {
      runInAction(() => {
        this.setLoading(false);
      });
    }
  };
}

const pokemonStore = new PokemonStore();
export default pokemonStore;
