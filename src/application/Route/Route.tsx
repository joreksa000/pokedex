import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import PokemonList from '../../persentasion/screen/main/PokePage';
import ComparePoke from '../../persentasion/screen/compare/ComparePoke';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import DetailPoke from '../../persentasion/screen/detail/DetailPoke';
import {Image, StyleSheet} from 'react-native';
import {responsiveHeight} from '../utils/Responsive';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const TabButton: React.FC = () => {
  const compares = require('../../persentasion/Assets/Image/pdxs.png');
  const pkeball = require('../../persentasion/Assets/Image/pkb.png');

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          elevation: 5,
          shadowColor: 'black',
          shadowOpacity: 1,
        },
      }}>
      <Tab.Screen
        name="Pokemon"
        component={PokemonList}
        options={{
          tabBarLabel: 'Pokemon',
          tabBarIcon: ({color, size}) => (
            <Image style={styles.PokemonTabs1} source={pkeball} />
          ),
          tabBarLabelStyle: {
            fontSize: responsiveHeight(14),
            marginBottom: 3,
          },
        }}
      />
      <Tab.Screen
        name="Comparison"
        component={ComparePoke}
        options={{
          tabBarLabel: 'Comparison',
          tabBarIcon: ({color, size}) => (
            <Image style={styles.PokemonTabs2} source={compares} />
          ),
          tabBarLabelStyle: {
            fontSize: responsiveHeight(14),
            marginBottom: 3,
          },
        }}
      />
    </Tab.Navigator>
  );
};
const StackScreen: React.FC = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Homes" component={TabButton} />
      <Stack.Screen name="Details" component={DetailPoke} />
    </Stack.Navigator>
  );
};
const Route: React.FC = () => {
  return (
    <NavigationContainer>
      <StackScreen />
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  PokemonTabs1: {
    height: responsiveHeight(45),
    width: responsiveHeight(45),
    resizeMode: 'contain',
  },
  PokemonTabs2: {
    height: responsiveHeight(35),
    width: responsiveHeight(35),
    resizeMode: 'contain',
  },
});

export default Route;
