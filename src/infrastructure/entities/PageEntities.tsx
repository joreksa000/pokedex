export interface Pokedex {
  name: string;
  url: string;
}

export interface Pokemon {
  name: string;
  url: string;
}

export interface DetailedPokemon {
  id: number;
  name: string;
  sprites: {
    front_default: string;
  };
  base_experience: number;
  order: number;
  height: number;
  weight: number;
  abilities: {ability: {name: string}}[];
  types: {type: {name: string}}[];
  stats: {
    base_stat: number;
    effort: number;
    stat: {
      name: string;
    };
  }[];
  cries: {
    latest: string;
    legacy: string;
  };
}

export interface ParamsRoot {
  navigate: any;
  goBack: any;
  push: any;
  params: any;
}
