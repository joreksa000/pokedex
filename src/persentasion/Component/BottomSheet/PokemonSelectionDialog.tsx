import React, {useMemo, useRef} from 'react';
import {
  View,
  Text,
  Button,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Pressable,
  ScrollView,
} from 'react-native';
import BottomSheet, {BottomSheetFlatList} from '@gorhom/bottom-sheet';
import {observer} from 'mobx-react';
import {DetailedPokemon} from '../../../infrastructure/entities/PageEntities';
import pokemonStore from '../../../application/Store/PokemonStore';
import {Capitalize} from '../../../application/utils/Capital';
import {responsiveHeight} from '../../../application/utils/Responsive';

interface PokemonSelectionDialogProps {
  isVisible: boolean;
  onClose: () => void;
  onSelectPokemon: (pokemon: DetailedPokemon) => void;
}

const PokemonSelectionDialog: React.FC<PokemonSelectionDialogProps> = observer(
  ({isVisible, onClose, onSelectPokemon}) => {
    const bottomSheetRef = useRef<BottomSheet>(null);
    const snapPoints = useMemo(() => ['25%', '50%', '90%'], []);

    const handleSelectPokemon = (pokemon: DetailedPokemon) => {
      onSelectPokemon(pokemon);
      onClose();
    };

    const renderPokemonItem = ({item}: {item: DetailedPokemon}) => {
      return (
        <TouchableOpacity
          style={styles.pokemonItem}
          onPress={() => handleSelectPokemon(item)}>
          <Text style={styles.pokemonName}>{Capitalize(item.name)}</Text>
        </TouchableOpacity>
      );
    };

    if (!isVisible) {
      return null;
    }

    return (
      <BottomSheet
        ref={bottomSheetRef}
        index={1}
        snapPoints={snapPoints}
        onClose={onClose}>
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.title}>Choose Pokemon</Text>
            <Pressable onPress={onClose} style={styles.button1}>
              <Text style={[styles.pokemonName, {color: 'white'}]}>Close</Text>
            </Pressable>
          </View>
          {pokemonStore.isLoading ? (
            <ActivityIndicator size="large" color="#0000ff" />
          ) : (
            <BottomSheetFlatList
              showsVerticalScrollIndicator={false}
              data={pokemonStore.comparisonPokemonList}
              keyExtractor={item => item.id.toString()}
              renderItem={renderPokemonItem}
            />
          )}
        </View>
      </BottomSheet>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  pokemonItem: {
    flex: 1,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  pokemonName: {
    fontSize: 18,
  },
  button1: {
    backgroundColor: '#647571',
    padding: responsiveHeight(10),
    borderRadius: 10,
  },
});

export default PokemonSelectionDialog;
