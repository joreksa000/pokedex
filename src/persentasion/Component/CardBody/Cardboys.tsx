import {View, Text, StyleSheet, Image} from 'react-native';
import React from 'react';
import {responsiveHeight} from '../../../application/utils/Responsive';
import {Decimetres, Hectograms} from '../../../application/utils/units';
import typeLogos from '../../../application/utils/TypeLogos';
import pokeColor from '../../Assets/colors/PokeColors';
import {Capitalize} from '../../../application/utils/Capital';
import {DetailedPokemon} from '../../../infrastructure/entities/PageEntities';
import PokemonStats from '../chart/Chartcom';

const Cardboys = ({item}: {item: DetailedPokemon}) => {
  return (
    <View style={styles.container}>
      <View style={styles.containerWH}>
        <Text style={styles.HWtxt}>{Hectograms(item.weight)}kg</Text>
        <Text style={styles.unitTxt}>WEIGHT</Text>
      </View>

      <View style={styles.tipeCon}>
        {item.types.map((type: any, index: any) => {
          const TypeLogo = typeLogos[type.type.name as keyof typeof typeLogos];
          return (
            <View style={{alignItems: 'center'}} key={index}>
              <View
                key={type.type.name}
                style={[
                  styles.typeBadgeContainer,
                  {
                    backgroundColor:
                      pokeColor[type.type.name as keyof typeof pokeColor],
                  },
                ]}>
                <TypeLogo width={13} height={13} />
              </View>
              <Text style={styles.textType}>{Capitalize(type.type.name)}</Text>
            </View>
          );
        })}
      </View>
      <View style={styles.containerWH}>
        <Text style={styles.HWtxt}>{Decimetres(item.height)}m</Text>
        <Text style={styles.unitTxt}>HEIGHT</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    borderBottomWidth: 0.2,
    padding: responsiveHeight(30),
    alignItems: 'center',
    marginTop: responsiveHeight(10),
    borderColor: '#aab1b1',
  },
  containerWH: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: responsiveHeight(10),
  },
  unitTxt: {
    fontSize: responsiveHeight(14),
    color: '#aab1b1',
    fontWeight: 'bold',
  },
  tipeCon: {
    flexDirection: 'row',
    borderRightWidth: 0.2,
    borderLeftWidth: 0.2,
    borderColor: '#aab1b1',
    paddingHorizontal: responsiveHeight(30),
  },
  typeBadgeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10,
    padding: 5,
    elevation: 5,
    shadowColor: 'black',
    shadowOpacity: 1,
    marginHorizontal: responsiveHeight(10),
  },
  textType: {
    color: '#aab1b1',
    fontWeight: 'bold',
    fontSize: responsiveHeight(14),
    marginLeft: 2,
  },
  HWtxt: {
    color: '#647571',
    fontSize: responsiveHeight(22),
    fontWeight: 'bold',
  },
});

export default Cardboys;
