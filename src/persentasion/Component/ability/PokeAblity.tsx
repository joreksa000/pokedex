import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {responsiveHeight} from '../../../application/utils/Responsive';
import axios from 'axios';
import {Capitalize} from '../../../application/utils/Capital';

interface Ability {
  name: string;
  url: string;
}

interface AbilityInfo {
  ability: Ability;
  is_hidden: boolean;
  slot: number;
}

interface PokemonAbilitiesProps {
  abilities: AbilityInfo[];
}

const PokemonAbilities: React.FC<PokemonAbilitiesProps> = ({abilities}) => {
  const [abilityDescriptions, setAbilityDescriptions] = useState<{
    [key: string]: string;
  }>({});

  useEffect(() => {
    const fetchAbilityDescriptions = async () => {
      const descriptions: {[key: string]: string} = {};

      await Promise.all(
        abilities.map(async abilityInfo => {
          try {
            const response = await axios.get(abilityInfo.ability.url);
            const description = response.data.effect_entries.find(
              (entry: {language: {name: string}}) =>
                entry.language.name === 'en',
            ).short_effect;
            descriptions[abilityInfo.ability.name] = description;
          } catch (error) {
            console.error('Failed to fetch ability description:', error);
          }
        }),
      );

      setAbilityDescriptions(descriptions);
    };

    fetchAbilityDescriptions();
  }, [abilities]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Abilities</Text>
      {abilities.map((abilityInfo, index) => (
        <View key={index} style={styles.abilityContainer}>
          <Text style={styles.abilityName}>
            {Capitalize(abilityInfo.ability.name)}
          </Text>
          <Text style={styles.abilityDescription}>
            {abilityDescriptions[abilityInfo.ability.name]}
          </Text>
          {abilityInfo.is_hidden && (
            <Text style={styles.isHidden}>Hidden Ability</Text>
          )}
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: responsiveHeight(10),
  },
  title: {
    fontSize: responsiveHeight(18),
    fontWeight: 'bold',
    marginBottom: 5,
  },
  abilityContainer: {
    marginBottom: responsiveHeight(10),
  },
  abilityName: {
    fontSize: responsiveHeight(20),
    fontWeight: 'bold',
    color: '#647571',
    marginBottom: 3,
  },
  abilityDescription: {
    fontSize: responsiveHeight(16),
    fontWeight: '600',
    marginBottom: 5,
  },
  isHidden: {
    fontStyle: 'italic',
    color: '#666',
  },
});

export default PokemonAbilities;
