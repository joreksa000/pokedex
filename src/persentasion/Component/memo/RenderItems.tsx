import {
  View,
  Text,
  Pressable,
  ImageBackground,
  Image,
  StyleSheet,
} from 'react-native';
import React from 'react';
import {
  DetailedPokemon,
  ParamsRoot,
} from '../../../infrastructure/entities/PageEntities';
import {useNavigation} from '@react-navigation/native';
import pokeColor from '../../Assets/colors/PokeColors';
import {responsiveHeight} from '../../../application/utils/Responsive';
import {Capitalize} from '../../../application/utils/Capital';
import typeLogos from '../../../application/utils/TypeLogos';

const RenderItems = ({item}: {item: DetailedPokemon}) => {
  const navigation = useNavigation<ParamsRoot>();
  const UpperName = item.name;
  const maps = item.types.map(t => t.type.name);
  const datas = maps[0].toLowerCase();

  const goToDetail = () => {
    navigation.navigate('Details', {data: item});
  };

  return (
    <Pressable
      onPress={goToDetail}
      style={[
        styles.itemContainer,
        {backgroundColor: pokeColor[datas as keyof typeof pokeColor]},
      ]}>
      <View style={styles.namesType}>
        <Text style={[styles.itemText, {color: 'white'}]}>
          {Capitalize(UpperName)}
        </Text>
        <Text style={[styles.itemText, {color: 'rgba(255, 255, 255, 0.5)'}]}>
          # {item.order}
        </Text>
      </View>
      <View style={{alignItems: 'center'}}>
        <ImageBackground
          style={styles.imgBG}
          source={require('../../Assets/Image/logo.png')}>
          <Image
            source={{uri: item.sprites.front_default}}
            style={styles.image}
          />
        </ImageBackground>
        <View style={styles.wraps}>
          <View style={styles.conType}>
            {item.types.map((type, index) => {
              const TypeLogo =
                typeLogos[type.type.name as keyof typeof typeLogos];
              return (
                <View
                  key={type.type.name}
                  style={[
                    styles.typeBadgeContainer,
                    {
                      backgroundColor:
                        pokeColor[type.type.name as keyof typeof pokeColor],
                    },
                  ]}>
                  <TypeLogo width={10} height={10} />
                  <Text style={styles.textType}>
                    {Capitalize(type.type.name)}
                  </Text>
                </View>
              );
            })}
          </View>
        </View>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    marginTop: responsiveHeight(8),
    flex: 1,
    margin: responsiveHeight(5),
    padding: responsiveHeight(10),
    borderRadius: 10,
    width: responsiveHeight(150),
    elevation: 10,
    shadowColor: 'black',
    shadowOpacity: 1,
  },
  imgBG: {
    width: responsiveHeight(150),
    height: responsiveHeight(150),
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: responsiveHeight(110),
    height: responsiveHeight(110),
  },
  itemText: {
    fontSize: responsiveHeight(20),
    textAlign: 'center',
    fontWeight: 'bold',
  },
  namesType: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  conType: {
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-evenly',
    borderRadius: 10,
  },
  textType: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: responsiveHeight(14),
    fontStyle: 'italic',
    marginLeft: 2,
  },
  typeBadgeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    padding: 5,
    elevation: 5,
    shadowColor: 'black',
    shadowOpacity: 1,
    marginBottom: responsiveHeight(4),
  },
  wraps: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    // alignItems: 'center',
    padding: 5,
    borderRadius: 10,
  },
  error: {
    color: 'red',
    fontSize: responsiveHeight(18),
    textAlign: 'center',
    marginTop: responsiveHeight(20),
  },
});
export default RenderItems;
