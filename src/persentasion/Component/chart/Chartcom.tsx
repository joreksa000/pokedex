import React from 'react';
import {View, Text, Dimensions, StyleSheet} from 'react-native';
import {BarChart} from 'react-native-chart-kit';
import {responsiveHeight} from '../../../application/utils/Responsive';

const screenWidth = Dimensions.get('window').width;

interface PokemonStatsProps {
  stats: {
    hp: number;
    attack: number;
    defense: number;
    specialAttack: number;
    specialDefense: number;
    speed: number;
  };
}

const PokemonStats: React.FC<PokemonStatsProps> = ({stats}) => {
  const data = {
    labels: ['HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed'],
    datasets: [
      {
        data: [
          stats.hp,
          stats.attack,
          stats.defense,
          stats.specialAttack,
          stats.specialDefense,
          stats.speed,
        ],
      },
    ],
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Pokémon Stats</Text>
      <BarChart
        data={data}
        width={screenWidth - 40}
        height={220}
        fromZero={true}
        yAxisLabel=""
        yAxisSuffix=""
        chartConfig={{
          backgroundColor: '#0000FF',
          backgroundGradientFrom: '#3b3b98',
          backgroundGradientTo: '#3498db',
          decimalPlaces: 0,
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForBackgroundLines: {
            strokeDasharray: '', // solid background lines with no dashes
          },
        }}
        style={{
          marginVertical: 8,
          borderRadius: 16,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginVertical: responsiveHeight(10),
  },
  title: {
    fontSize: responsiveHeight(18),
    fontWeight: 'bold',
  },
});

export default PokemonStats;
