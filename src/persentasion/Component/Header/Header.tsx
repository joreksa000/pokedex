import {View, Text, StyleSheet, Image} from 'react-native';
import React from 'react';
import {responsiveHeight} from '../../../application/utils/Responsive';

interface Properti {
  name: string;
}

const Header: React.FC<Properti> = ({name}) => {
  const dexs = require('../../Assets/Image/star.png');

  return (
    <View style={styles.container}>
      <Image
        source={dexs}
        style={{
          resizeMode: 'contain',
          height: responsiveHeight(35),
          width: responsiveHeight(35),
          marginRight: 10,
        }}
      />
      <Text
        style={{
          color: 'white',
          fontSize: responsiveHeight(20),
          fontWeight: 'bold',
        }}>
        {name} - Joda Reksa
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#142247',
    height: responsiveHeight(65),
    elevation: 5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
});

export default Header;
