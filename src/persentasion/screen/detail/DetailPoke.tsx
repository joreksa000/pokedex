import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  Image,
  Pressable,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import {useRoute} from '@react-navigation/native';
import Header from '../../Component/Header/Header';
import {responsiveHeight} from '../../../application/utils/Responsive';
import {SvgUri} from 'react-native-svg';
import {Capitalize} from '../../../application/utils/Capital';
import Cardboys from '../../Component/CardBody/Cardboys';
import PokemonStats from '../../Component/chart/Chartcom';
import PokemonAbilities from '../../Component/ability/PokeAblity';

const DetailPoke = () => {
  const [changes, setChanges] = useState(true);

  const route = useRoute<any>();
  const {data} = route.params;

  const bgxs = require('../../Assets/Image/bgx.jpeg');
  const ButtonChange = require('../../Assets/Image/iconschange.png');

  const UpperName = data.name;
  const Sprites = data.sprites.other['home'].front_default;
  const Dreams = data.sprites.other['dream_world'].front_default;
  const HPpanel = data.stats[0].stat.name;
  return (
    <ImageBackground source={bgxs} style={styles.bgStyle} resizeMode="cover">
      <View style={{flex: 1}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.pokecontainer}>
            <Pressable
              style={styles.buttonChanges}
              onPress={() => {
                setChanges(!changes);
              }}>
              <Image source={ButtonChange} style={styles.buttonsChanges} />
            </Pressable>
            {changes === true ? (
              <SvgUri
                width={responsiveHeight(250)}
                height={responsiveHeight(300)}
                uri={`${Dreams}`}
                style={{top: responsiveHeight(15)}}
              />
            ) : (
              <Image source={{uri: Sprites}} style={styles.pokes} />
            )}
          </View>
          <View style={styles.cardsWhite}>
            <Text style={styles.txtName}>{Capitalize(UpperName)}</Text>
            <View style={styles.HealthBar} />
            <View style={styles.containHeal}>
              <Text style={[styles.healp, {marginRight: responsiveHeight(5)}]}>
                {`${data.stats[0].base_stat}/${data.stats[0].base_stat}`}
              </Text>
              <Text style={styles.healp}>{Capitalize(HPpanel)}</Text>
            </View>
            <Cardboys item={data} />
            <View style={{marginBottom: responsiveHeight(20)}}>
              <PokemonStats
                stats={{
                  hp: data.stats[0].base_stat,
                  attack: data.stats[1].base_stat,
                  defense: data.stats[2].base_stat,
                  specialAttack: data.stats[3].base_stat,
                  specialDefense: data.stats[4].base_stat,
                  speed: data.stats[5].base_stat,
                }}
              />
            </View>
            <View style={styles.conAbs}>
              <PokemonAbilities abilities={data.abilities} />
            </View>
          </View>
        </ScrollView>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  bgStyle: {
    flex: 1,
  },
  cardsWhite: {
    backgroundColor: 'white',
    alignItems: 'center',
    width: '95%',
    flex: 2,
    borderTopStartRadius: 10,
    borderTopEndRadius: 10,
    elevation: 5,
    alignSelf: 'center',
  },
  pokecontainer: {
    flex: 1,
    alignItems: 'center',
  },
  buttonsChanges: {
    resizeMode: 'contain',
    height: responsiveHeight(40),
    width: responsiveHeight(40),
  },
  pokes: {
    width: responsiveHeight(300),
    height: responsiveHeight(300),
    resizeMode: 'contain',
    overflow: 'visible',
  },
  buttonChanges: {
    alignSelf: 'flex-end',
    marginHorizontal: responsiveHeight(50),
    marginVertical: responsiveHeight(50),
    position: 'absolute',
    right: responsiveHeight(2),
    borderWidth: 0.5,
    borderColor: '#8eecbf',
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    borderRadius: 20,
  },
  HealthBar: {
    height: responsiveHeight(8),
    width: responsiveHeight(180),
    borderRadius: 10,
    backgroundColor: '#8eecbf',
    marginTop: responsiveHeight(10),
  },
  txtName: {
    fontSize: responsiveHeight(22),
    fontWeight: 'bold',
    marginTop: responsiveHeight(10),
  },
  containHeal: {flexDirection: 'row', marginTop: responsiveHeight(5)},
  healp: {
    fontSize: responsiveHeight(20),
    fontWeight: '600',
  },
  conAbs: {
    width: '100%',
    paddingHorizontal: responsiveHeight(15),
  },
});

export default DetailPoke;
