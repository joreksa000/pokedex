import React, {useEffect, useState} from 'react';
import {observer} from 'mobx-react';
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  ImageBackground,
  Alert,
  BackHandler,
  Button,
} from 'react-native';
import pokemonStore from '../../../application/Store/PokemonStore';
import {responsiveHeight} from '../../../application/utils/Responsive';
import Header from '../../Component/Header/Header';
import RenderItems from '../../Component/memo/RenderItems';
import {DetailedPokemon} from '../../../infrastructure/entities/PageEntities';
import PokemonSelectionDialog from '../../Component/BottomSheet/PokemonSelectionDialog';

const PokemonList = observer(() => {
  const bgxs = require('../../Assets/Image/bgx.jpeg');

  useEffect(() => {
    pokemonStore.fetchPokemon();
    pokemonStore.fetchComparisonPokemonList();
  }, []);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want to exit the app?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'YES',
          onPress: () => BackHandler.exitApp(),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const handleEndReached = () => {
    if (!pokemonStore.isLoading) {
      pokemonStore.incrementPage();
      pokemonStore.fetchPokemon();
    }
  };

  if (pokemonStore.isLoading && pokemonStore.currentPage === 1) {
    return <ActivityIndicator size="large" color="#0000ff" />;
  }

  if (pokemonStore.error) {
    return <Text style={styles.error}>{pokemonStore.error}</Text>;
  }

  return (
    <View style={styles.contain}>
      <Header name="PokeDex" />
      <ImageBackground source={bgxs} resizeMode="cover">
        <FlatList
          data={pokemonStore.pokemonList}
          keyExtractor={item => item.id?.toString()}
          renderItem={item => {
            return <RenderItems item={item.item} />;
          }}
          numColumns={3}
          showsVerticalScrollIndicator={false}
          columnWrapperStyle={styles.row}
          onEndReached={handleEndReached}
          onEndReachedThreshold={0.5}
          ListFooterComponent={
            pokemonStore.isLoading ? (
              <ActivityIndicator size="large" color="#0000ff" />
            ) : null
          }
        />
      </ImageBackground>
    </View>
  );
});

const styles = StyleSheet.create({
  contain: {
    backgroundColor: 'white',
    flex: 1,
  },
  row: {
    flex: 1,
    justifyContent: 'space-around',
  },
  error: {
    color: 'red',
    fontSize: responsiveHeight(18),
    textAlign: 'center',
    marginTop: responsiveHeight(20),
  },
});

export default PokemonList;
