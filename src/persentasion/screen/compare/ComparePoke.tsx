import {
  View,
  Text,
  ImageBackground,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Header from '../../Component/Header/Header';
import pokemonStore from '../../../application/Store/PokemonStore';
import {Picker} from '@react-native-picker/picker';
import {DetailedPokemon} from '../../../infrastructure/entities/PageEntities';
import PokemonStats from '../../Component/chart/Chartcom';
import {Capitalize} from '../../../application/utils/Capital';
import {responsiveHeight} from '../../../application/utils/Responsive';
import pokeColor from '../../Assets/colors/PokeColors';
import PokemonSelectionDialog from '../../Component/BottomSheet/PokemonSelectionDialog';

const ComparePoke = () => {
  const [isDialogVisible, setIsDialogVisible] = useState(false);
  const [isDialogVisible2, setIsDialogVisible2] = useState(false);

  const [selectedPokemon1, setSelectedPokemon1] =
    useState<DetailedPokemon | null>(null);
  const [selectedPokemon2, setSelectedPokemon2] =
    useState<DetailedPokemon | null>(null);

  const bgxs = require('../../Assets/Image/bgx.jpeg');

  const openDialog = () => {
    setIsDialogVisible(true);
    setSelectedPokemon1(null);
  };
  const openDialog2 = () => {
    setIsDialogVisible2(true);
    setSelectedPokemon2(null);
  };

  const closeDialog = () => {
    setIsDialogVisible(false);
  };
  const closeDialog2 = () => {
    setIsDialogVisible2(false);
  };

  const handleSelectPokemon = (pokemon: DetailedPokemon) => {
    if (!selectedPokemon1) {
      setSelectedPokemon1(pokemon);
      setIsDialogVisible(false);
    } else if (!selectedPokemon2) {
      setSelectedPokemon2(pokemon);
      setIsDialogVisible(false);
    }
  };

  const renderPokemonDetails = (pokemon: DetailedPokemon | null) => {
    if (!pokemon) return;
    const Sprites = pokemon.sprites.front_default;
    const maps = pokemon.types.map(t => t.type.name);
    const datas = maps[0].toLowerCase();
    return (
      <View
        style={[
          styles.pokemonCard,
          {backgroundColor: pokeColor[datas as keyof typeof pokeColor]},
        ]}>
        <Text style={styles.pokemonName}>{Capitalize(pokemon.name)}</Text>
        <ImageBackground
          style={styles.imgBG}
          source={require('../../Assets/Image/logo.png')}>
          <Image source={{uri: Sprites}} style={styles.image} />
        </ImageBackground>
        <PokemonStats
          stats={{
            hp: Number(
              pokemon.stats.find(stat => stat.stat.name === 'hp')?.base_stat,
            ),
            attack: Number(
              pokemon.stats.find(stat => stat.stat.name === 'attack')
                ?.base_stat,
            ),
            defense: Number(
              pokemon.stats.find(stat => stat.stat.name === 'defense')
                ?.base_stat,
            ),
            specialAttack: Number(
              pokemon.stats.find(stat => stat.stat.name === 'special-attack')
                ?.base_stat,
            ),
            specialDefense: Number(
              pokemon.stats.find(stat => stat.stat.name === 'special-defense')
                ?.base_stat,
            ),
            speed: Number(
              pokemon.stats.find(stat => stat.stat.name === 'speed')?.base_stat,
            ),
          }}
        />
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      <Header name={'Comparison'} />
      <ImageBackground source={bgxs} style={{flex: 1}} resizeMode="cover">
        <View style={styles.selects}>
          <TouchableOpacity style={styles.Selects} onPress={openDialog}>
            <Text style={styles.chose}>
              {selectedPokemon1
                ? Capitalize(selectedPokemon1.name)
                : 'Chose Pokémon'}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.Selects} onPress={openDialog2}>
            <Text style={styles.chose}>
              {selectedPokemon2
                ? Capitalize(selectedPokemon2.name)
                : 'Chose Pokémon'}
            </Text>
          </TouchableOpacity>
        </View>

        {selectedPokemon1 && selectedPokemon2 && (
          <>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.pokemonCard}>
                {renderPokemonDetails(selectedPokemon1)}
              </View>
              <View style={styles.pokemonCard}>
                {renderPokemonDetails(selectedPokemon2)}
              </View>
            </ScrollView>
          </>
        )}
        <PokemonSelectionDialog
          isVisible={isDialogVisible}
          onClose={closeDialog}
          onSelectPokemon={handleSelectPokemon}
        />
        <PokemonSelectionDialog
          isVisible={isDialogVisible2}
          onClose={closeDialog2}
          onSelectPokemon={handleSelectPokemon}
        />
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  pickerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
  selects: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: responsiveHeight(5),
  },
  Selects: {
    backgroundColor: '#647571',
    padding: responsiveHeight(10),
    marginTop: responsiveHeight(5),
    borderRadius: 10,
  },
  chose: {
    color: 'white',
    fontSize: responsiveHeight(16),
    fontWeight: 'bold',
  },
  pokemonCard: {
    alignItems: 'center',
    padding: responsiveHeight(10),
    borderRadius: 8,
    width: '100%',
  },
  pokemonName: {
    color: 'black',
    fontSize: responsiveHeight(22),
    fontWeight: 'bold',
    marginBottom: responsiveHeight(8),
  },
  imgBG: {
    width: responsiveHeight(150),
    height: responsiveHeight(150),
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: responsiveHeight(110),
    height: responsiveHeight(110),
  },
});

export default ComparePoke;
